Below we illustrate, both in Latin and in non-Latin script, uses of the \LaTeX~commands defined for inline examples, together with the corresponding source codes.\footnote{The definitions of these commands can be found in the \texttt{multilingual-mwe-example.tex} file attached to these guidelines.} Many of these examples omit important elements such as translations or exceed one line of text. Such incomplete or long inline examples are discouraged, and they are given here for the sake of completeness only. Whenever possible, numbered examples are preferred.

As mentioned above, a full-fledged inline example contains at least 5 elements: a sample use of a MWE, its transliteration, gloss, literal translation and idiomatic translation. A command is defined for each of these elements, in case they are to appear in isolation:

\begin{itemize}
\item inline example in Latin script: \\
(de) \ile{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}} 
\vspace{-0.38cm}
\begin{spverbatim}
\ile{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}
\end{spverbatim}

\item inline example in non-Latin script: \\
(el) \ile[nl]{\lex{Ρίχνω τα μούτρα} μου}
\vspace{-0.38cm}
\begin{spverbatim}
\ile[nl]{\lex{Ρίχνω τα μούτρα} μου}
\end{spverbatim}

\item transliteration alone:\\
`\tli{Richno ta mutra mu}'
\vspace{-0.38cm}
\begin{spverbatim}
`\tli{Richno ta mutra mu}'
\end{spverbatim}

\item gloss alone: \\
`\glo{Chris has Alex the head washed}'
\vspace{-0.38cm}
\begin{spverbatim}
`\glo{Chris has Alex the head washed}'
\end{spverbatim}

\item literal translation alone: \\
\lit{Chris washed Alex' head}
\vspace{-0.38cm}
\begin{spverbatim}
\lit{Chris washed Alex' head}
\end{spverbatim}

\item idiomatic translation alone: \\
\idio{Chris gave Alex a telling-off}
\vspace{-0.38cm}
\begin{spverbatim}
\idio{Chris gave Alex a telling-off}
\end{spverbatim}

\end{itemize}

For a complete inline example, the \verb!\mwex! command is to be used, with all five elements passed as parameters:

\begin{itemize}
\item inline example in non-Latin script, transliteration, gloss, literal and idiomatic translation: \\
(el) \mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{Richno ta mutra mu}{drop-\feats{1sg} the face \feats{poss.1sg}}{I drop my face}{I suppress my dignity}
\vspace{-0.38cm}
\begin{spverbatim}
\mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{Richno ta mutra mu}{drop-\feats{1sg} the face \feats{poss.1sg}}{I drop my face}{I suppress my dignity}
\end{spverbatim}
\end{itemize}

Most often, some of these elements will be skipped, e.g.~because no transliteration is needed or to avoid examples spreading over more than one line.
Skipping an element boils down to replacing the corresponding parameter by an empty string (note that all 5 pairs of braces have to appear, even if some parameters are empty). Here are examples of inline examples with skipped parameters:

\begin{itemize}

\item inline example in Latin script and gloss: \\
(de) \mwex{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{Chris has Alex the head  washed}{}{}
\vspace{-0.38cm}
\begin{spverbatim}
\mwex{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{Chris has Alex the head  washed}{}{}
\end{spverbatim}

\item inline example in non-Latin script, transliteration and gloss: \\
(el) \mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{Richn-o ta mutra mu}{drop-\feats{1sg} the face \feats{poss.1sg}}{}{}
\vspace{-0.9cm}
\begin{spverbatim}
\mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{Richn-o ta mutra mu}{drop-\feats{1sg} the face \feats{poss.1sg}}{}{}
\end{spverbatim}

\item inline example in Latin script, literal and idiomatic translation: \\
(de) \mwex{Chris hat Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{}{Chris washed Alex' head}{Chris gave Alex a telling-off}
\vspace{-0.38cm}
\begin{spverbatim}
\mwex{Chris hat Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{}{Chris washed Alex' head}{Chris gave Alex a telling-off}
\end{spverbatim}

\item inline example in non-Latin script, gloss, literal and idiomatic translation: \\
(el) \mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{}{drop-\feats{1sg} the face \feats{poss.1sg}}{I drop my face}{I suppress my dignity}
\vspace{-0.38cm}
\begin{spverbatim}
\mwex[nl]{\lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου}{}{drop-\feats{1sg} the face \feats{poss.1sg}}{I drop my face}{I suppress my dignity}
\end{spverbatim}

\end{itemize}

For convenience, custom commands can be defined for frequently used variants of \verb!\mwex!. For instance, for inline examples with literal and idiomatic translation, the following definition could be used:

\vspace{-0.38cm}
\begin{spverbatim}
\newcommand{\exlitidio}[4][]{\mwex[#1]{#2}{}{}{#3}{#4}}
\end{spverbatim}

\begin{itemize}
\item inline example with the custom command  \verb!\exlitidio!:\\
(de) \exlitidio{Chris hat Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{Chris washed Alex' head}{Chris gave Alex a telling-off}
\vspace{-0.38cm}
\begin{spverbatim}
\exlitidio{Chris hat Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{Chris washed Alex' head}{Chris gave Alex a telling-off}
\end{spverbatim}

\end{itemize}





