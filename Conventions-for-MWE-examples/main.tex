\documentclass[output=paper,colorlinks,citecolor=brown]{langscibook}

\input{multilingual-mwe-examples.tex} % PMWE-specific commands for MWE examples

\newfontfamily\Parsifont[Script=Arabic]{ScheherazadeRegOT_Jazm.ttf} % Fonts for Farsi
\newcommand{\PRL}[1]{\RL{\Parsifont #1}} % Shortcut for right-to-left (\RL) Farsi text
\newcommand\blfootnote[1]{%
  \begingroup
  \renewcommand\thefootnote{}\footnote{#1}%
  \addtocounter{footnote}{-1}%
  \endgroup
}
    
\usepackage{enumitem}
\usepackage{spverbatim}

\bibliography{localbibliography}

%\frontmatter

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\title{PMWE conventions for examples containing multiword expressions}

\author{
 Stella Markantonatou\affiliation{Institute for Language and Speech Processing, Athena RIC, Greece}\and 
 Carlos Ramisch\affiliation{Aix-Marseille University, CNRS, LIS, France}\and
 Victoria Rosén\affiliation{University of Bergen, Norway}\and
 Mike Rosner\affiliation{University of Malta, Malta}\and
 Manfred Sailer\affiliation{Goethe University Frankfurt a.M., Germany}\and
 Agata Savary\affiliation{University of Tours, LIFAT, France}\lastand
 Veronika Vincze\affiliation{University of Szeged, Hungary}
}

\abstract{}
\papernote{}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\begin{document}

\maketitle
\rohead{{\small Conventions for citing and glossing multilingual examples of MWEs}}
\lehead{{\small S. Markantonatou, C. Ramisch, V. Rosén, M. Rosner, M. Sailer, A. Savary \& V. Vincze}}

\blfootnote{Published under the Creative Commons Attribution 4.0 Licence (CC BY 4.0):
http://creativecommons.org/licenses/by/4.0/}

Publications about multiword expressions (MWEs) may address a large number of languages, as in the case of books published in the \textit{Phraseology and Multiword Expressions} (PMWE) series at \textit{Language Science Press} (LSP). The present document extends the LSP conventions for citing and glossing multilingual examples so as to adapt them to MWEs, based on the notation conventions presented in the preface of PMWE volume 2 by \citet{Markantonatou:pmwe:2018}.\footnote{\url{https://langsci-press.org/catalog/view/204/1658/1311-1}}

We put forward notational conventions which could become a standard for citing and glossing multilingual MWE examples, both in the PMWE series and in other publications.
Special attention is given to MWEs and their glosses and idiomatic translations. Please try to follow these conventions as far as possible in your PMWE volume and/or chapter.

We distinguish \emph{numbered examples}, appearing as separate elements, from \emph{inline examples}, appearing directly in the text body. We illustrate the proposed conventions for numbered examples in (\ref{de:kopf-waschen})--(\ref{en:pull-strings}) below. Each numbered example may contain:
\begin{enumerate}[label=\alph*]

\item\label{ex-line:use} a sample use of the MWE; 

\item\label{ex-line:lang} in the same line, the language name, or its  ISO 639 code in lowercase, parenthesized and right aligned;\footnote{The language name or code only needs to be specified when it is not clear from the text what language the example is in. Authors should choose a convention, either the full language name or its two- or three-letter ISO 639 code, and use it consistently throughout each chapter. In special cases, more precise Glottolog language codes may also be used.}

\item\label{ex-line:transliteration} a transliteration, if the language of the example is written with a script other than the one used for the main text;\footnote{In PMWE volumes, which are always written in English (Latin script), transliteration is needed e.g. for Bulgarian, Greek, Farsi and Hebrew examples. Conversely, in other publications possibly written in Cyrillic, Greek, Arabic or Hebrew, transliterations would be required for examples in English, or any other language using Latin script.}

\item\label{ex-line:gloss} a gloss following the Leipzig Glossing Rules;\footnote{\url{https://www.eva.mpg.de/lingua/resources/glossing-rules.php}} 

\item\label{ex-line:lit-trans} a literal translation in single quotes, preceded by \textit{lit.}; 

\item\label{ex-line:idio-trans} an idiomatic translation in single quotes;

\item\label{ex-line:source} the source of the example and of its translation, parenthesized and right-aligned, whenever available. 

\end{enumerate}

The German example in (\ref{de:kopf-waschen}) displays all the above items except for \ref{ex-line:transliteration}, since transliteration is not relevant for languages written in the Latin alphabet.

\ea\label{de:kopf-waschen}
\settowidth \jamwidth{(de)} 
\gll Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}.\\
    Chris has Alex the  head  washed \\  \jambox{(de)}
\glt lit. `Chris washed Alex' head.'\\
`Chris gave Alex a telling-off.' \\
\hspace*{\fill}(Höhle 2018: 9; our gloss and translation)
\z

The Farsi example in (\ref{fa:have-sleep-for-sb}) displays all items \ref{ex-line:use}–\ref{ex-line:idio-trans}. Item \ref{ex-line:source} may be omitted when the source of the example is not known, or when the example has been constructed. 
Items \ref{ex-line:lit-trans} and \ref{ex-line:idio-trans} can appear on the same line, separated by a vertical bar `|'.

\ea \label{fa:have-sleep-for-sb}
\settowidth \jamwidth{(fa)} 
\glll \PRL{است} \lex{\PRL{دیده}} \lex{\PRL{خواب}} \PRL{من} \lex{\PRL{برای}} \PRL{کافی} \PRL{قدر} \PRL{به} \\
    ast \lex{dide} \lex{khab} man \lex{baraye} kafi qadre be  \\
    is seen sleep me for enough quantity to\\ \jambox{(fa)}
\glt lit. `He had enough sleep for me.' | `He has many plans for me.’
\z

Farsi is a language which is written right to left, so the order of the words in item \ref{ex-line:use}, the Farsi sentence, is right to left. The transliteration in \ref{ex-line:transliteration}, however, is spelled left-to-right within each word.

The glosses for the examples in (\ref{de:kopf-waschen}) and (\ref{fa:have-sleep-for-sb}) are fully inflected English words. Sometimes it is desirable to use morpheme-by-morpheme glosses rather than inflected words, and the Leipzig glossing rules provide detailed instructions on how to do this. But the level of detail in the gloss should reflect the purpose of the example. When discussing MWEs, it is often irrelevant to include full morphological analyses of all the words in the sentence. In the Greek example in (\ref{el:drop-face}), only the person and number inflection of the verb and the possessive are shown; without this agreement the idiomatic reading of the MWE is lost.\footnote{The verb and its gloss could have been fully specified as \mwex[nl]{Ρίχν-ω}{}{drop.\feats{ipfv.prs.ind-1sg}}{}{}.}

\ea \label{el:drop-face}
\settowidth \jamwidth{(el)} 
\glll \lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου. \\
    Richn-o ta mutra mu\\
    drop-\feats{1sg} the face \feats{poss.1sg}\\ \jambox{(el)}
\glt lit. `I drop my face.' | `I suppress my dignity.'
\z

For English examples in English texts, most of the items \ref{ex-line:use}–\ref{ex-line:lit-trans} are irrelevant. 
Nonetheless, sometimes an idiomatic translation, item \ref{ex-line:idio-trans}, can be useful to ease comprehension for non-native readers, as in (\ref{en:pull-strings}).

\ea\label{en:pull-strings}
\settowidth \jamwidth{(en)} 
The company boss can still \lex{pull} the \lex{strings} from prison. \jambox{(en)}
\glt `The company boss can still exert hidden influence from prison.'
\z

In all these examples the lexicalized components of the MWE, i.e., those which are always realized by the same lexeme, are highlighted in boldface.\footnote{\url{https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.2/?page=lexicalized}} 

\emph{Inline examples}, used for brevity, are preceded by the lowercase two- or three-letter ISO 639 code in parentheses, and contain only a subset of items \ref{ex-line:use}--\ref{ex-line:idio-trans}.
Items \ref{ex-line:transliteration}--\ref{ex-line:lit-trans}, if present, occur in parentheses and are separated by a vertical bar `|'. Inline examples exceeding one line of text are discouraged.

If the language uses Latin script, item \ref{ex-line:use} in inline examples is always spelled in italics. 
For instance, an inline example corresponding to numbered example (\ref{de:kopf-waschen}) would be as follows: (de) \mwex{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{Chris has Alex the head  washed}{Chris washed Alex' head}{Chris gave Alex a telling-off}. The same example omitting the gloss would be (de) \mwex{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{}{}{Chris washed Alex' head}{Chris gave Alex a telling-off}.

If the language under study is written in non-Latin script,  
item \ref{ex-line:use} should not be in italics, and the transliteration should be included in parentheses (and followed by a vertical bar `|' if needed). For instance, the use example, 
transliteration, literal translation and idiomatic translation of example (\ref{el:drop-face}) would be: 
(el) \mwex[nl]{\lex{Ρίχνω τα μούτρα} μου}{Richno to mutra mu}{}{I drop my face}{I suppress my dignity}.

To keep such examples reasonably short, the first item can be omitted, keeping only the transliteration: 
(el) \mwex{\lex{Richno ta mutra} mu}{}{}{I drop my face}{I suppress my dignity}.
Literal and idiomatic translations  
may be too verbose or superfluous, and can be skipped: 
(el) \mwex[nl]{\lex{Ρίχνω τα μούτρα} μου}{}{}{}{I suppress my dignity}.

The \LaTeX~typesetting commands for both numbered and inline examples can be found in the LSP templates and in the source codes attached to these guidelines.\footnote{ \url{https://gitlab.com/parseme/pmwe/-/blob/master/Conventions-for-MWE-examples/multilingual-mwe-examples.tex}} Examples of use for these commands are given in Appendices A and B. 
These commands are to be accompanied by the Language Science Press templates.\footnote{\url{https://langsci-press.org/templatesAndTools}~~\url{https://github.com/langsci/langscibook/releases}}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\appendix

\section*{Appendix A: \LaTeX{} commands for numbered examples}
\input{app-numbered}

\section*{Appendix B: \LaTeX{} commands for inline examples}
\input{app-inline}

\end{document}

