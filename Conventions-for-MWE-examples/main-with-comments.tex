\documentclass[output=paper,colorlinks,citecolor=brown]{langsci/langscibook} 
\bibliography{localbibliography}

\newfontfamily\Parsifont[Script=Arabic]{ScheherazadeRegOT_Jazm.ttf} 
\newcommand{\PRL}[1]{\RL{\Parsifont #1}}

\input{localpackages.tex}
\input{localcommands.tex}
\input{multilingual-mwe-examples.tex} % <= Please input this file in your chapter to use the inline example commands

\lsCollectionPaperFrontmatterMode{}

%\title{PMWE conventions for citing and glossing multilingual examples of multiword expressions}  
%\title{PMWE conventions for examples of multiword expressions}  
\title{PMWE conventions for examples containing multiword expressions}  

\author{
 Stella Markantonatou\affiliation{Institute for Language and Speech Processing, Athena RIC, Greece}\and 
 Carlos Ramisch\affiliation{Aix-Marseille University, CNRS, LIS, France}\and
 Victoria Rosén\affiliation{University of Bergen, Norway}\and
 Mike Rosner\affiliation{University of Malta, Malta}\and
 Manfred Sailer\affiliation{Goethe University Frankfurt a.M., Germany}\and
 Agata Savary\affiliation{University of Tours, LIFAT, France}\lastand
 Veronika Vincze\affiliation{University of Szeged, Hungary}
}

\abstract{}
\papernote{}
%\abstract{This document extends the Language Science Press conventions for citing and glossing multilingual examples, so as to adapt them to multiword expressions.}

% \IfFileExists{../localcommands.tex}{%hack to check whether this is being compiled as part of a collection or standalone
%   \input{../localpackages}
%   \input{../localcommands} 
% \togglepaper[23]
% }{}

\begin{document}

\maketitle
\rohead{{\small Conventions for citing and glossing multilingual examples of MWEs}}
\lehead{{\small S. Markantonatou, C. Ramisch, V. Rosén, M. Rosner, M. Sailer, A. Savary \& V. Vincze}}

Publications about multiword expressions (MWEs) may address a large number of languages, as in the case of books published in the \textit{Phraseology and Multiword Expressions} (PMWE) series at \textit{Language Science Press} (LSP). The present document extends the LSP conventions for citing and glossing multilingual examples so as to adapt them to MWEs, based on the notation conventions presented in the preface of PMWE volume 2 by \citet{Markantonatou:pmwe:2018}.\footnote{\url{https://langsci-press.org/catalog/view/204/1658/1311-1}}

We put forward notational conventions which could become a standard for citing and glossing multilingual MWE examples, both in the PMWE series and in other publications.
Special attention is given to MWEs and their glosses and idiomatic translations. Please try to follow these conventions as far as possible in your PMWE volume and/or chapter.

% MODIFIED BY CARLOS ON Jun 09. Previous version below
%\abstract{This document extends the Language Science Press conventions for citing and glossing multilingual examples, so as to adapt them to multiword expressions (MWEs).}

%This document is based on the notation conventions presented in the preface of PMWE volume 2 by \citet{Markantonatou:pmwe:2018}. 
%%\todo{add a precise reference to the initial document} 
%The goal is to provide precise conventions on how to present linguistic examples in the PMWE series. Special attention is put on MWEs, their glosses and idiomatic translations. Please, try to follow these conventions as much as possible in your volume and/or chapter.

%Papers and chapters about multiword expressions may address a large number of languages, as in the case of books published in the \textit{Phraseology and Multiword Expressions} (PMWE) series at \textit{Language Science Press}. We put forward notational conventions which might become a standard for citing and glossing multilingual MWE examples, both in the PMWE series and in other publications.

We distinguish \emph{numbered examples}, appearing as separate elements, from \emph{inline examples}, appearing directly in the text body. We illustrate the proposed conventions for numbered examples in (\ref{de:kopf-waschen})--(\ref{en:take-on}) below. Each numbered example may contain:
\begin{enumerate}[label=\alph*]%[(i)]
%\item\label{ex-line:use} a sample use of the MWE, %VMWE
%followed by the language name or its two-letter ISO 639-1 language code in uppercase,\footnote{The language name or code only needs to be specified when it is not clear from the text what language the example is in. Authors should choose one of the two conventions, either the full language names or their ISO 639-1 codes, and use it consistently throughout each chapter.} parenthesised and right aligned; %%(cf.~\figref{fig:language-tree}),

\item\label{ex-line:use} a sample use of the MWE; %VMWE

\item\label{ex-line:lang} in the same line, the language name, or its  ISO 639 code in lowercase, parenthesized and right aligned;\footnote{The language name or code only needs to be specified when it is not clear from the text what language the example is in. Authors should choose a convention, either the full language name or its two- or three-letter ISO 639 code, and use it consistently throughout each chapter.}


\item\label{ex-line:transliteration} a transliteration, if the language of the example is written with a script other than the one used for the main text;\footnote{In PMWE volumes, which are always written in English (Latin script), transliteration is needed e.g. for Bulgarian, Greek, Farsi and Hebrew examples. Conversely, in other publications possibly written in Cyrillic, Greek, Arabic or Hebrew, transliterations would be required for examples in English, or any other language using Latin script.}
%\footnote{For instance, transliteration is needed for Bulgarian, Greek, Farsi and Hebrew examples.
%Conversely, examples in English, or any other language using Latin script, would require transliterations in texts written in Cyrillic, Greek, Arabic or Hebrew script. }

\item\label{ex-line:gloss} a gloss following the Leipzig Glossing Rules;\footnote{\url{https://www.eva.mpg.de/lingua/pdf/Glossing-Rules.pdf}} 

\item\label{ex-line:lit-trans} a literal translation in single quotes, preceded by \textit{lit.}; 

\item\label{ex-line:idio-trans} an idiomatic translation in single quotes;

\item\label{ex-line:source} the source of the example and of its translation, parenthesized and right-aligned, whenever available. 

\end{enumerate}

The German example in (\ref{de:kopf-waschen}) displays all the above items except for \ref{ex-line:transliteration}, since transliteration is not relevant for languages written in the Latin alphabet.

%For English examples, items \ref{ex-line:transliteration}--\ref{ex-line:lit-trans} are irrelevant, but item \ref{ex-line:idio-trans} might sometimes be useful to ease comprehension for non-native readers, as in example (\ref{en:take-on}). For brevity, items \ref{ex-line:lit-trans} and \ref{ex-line:idio-trans} can be combined into %contained in 
%a single line, and are then separated by a vertical bar `|', as in example (\ref{fa:have-sleep-for-sb}). %as in examples (\ref{el:drop-face}--\ref{fa:have-sleep-for-sb}). 
%For right-to-left languages (e.g., Farsi, Hebrew), item \ref{ex-line:use} is spelled right-to-left, \cara{items \ref{ex-line:lang}, \ref{ex-line:lit-trans} and \ref{ex-line:idio-trans} left-to-right}, and items \ref{ex-line:transliteration}--\ref{ex-line:gloss} left-to-right within components, and right-to-left from one component to another, as in example (\ref{fa:have-sleep-for-sb}). 
 %For instance in (\ref{el:drop-face}), the verb gloss (drop) is assigned person and number but not tense and mood, while the noun gloss (face) receives the case but not the number and the gender indication. 
%\cara{For instance, in (\ref{el:drop-face}), some morphological features of the verb gloss (\ile{drop}) are omitted because the focus is on the fixed noun (\ile{face}). \footnote{The verb and its gloss could have been fully specified as \exgl[nl]{Ρίχν-ω}{drop.\feats{imperf-pres.ind.1sg}}.}}
%\as{For instance, in (\ref{el:drop-face}), some morphological features of the verb gloss (\ile{drop}) are omitted because the focus is on the person and number agreement between the verb and the possessive (without the agreement the idiomatic reading is lost).}\footnote{The verb and its gloss could have been fully specified as \exgl[nl]{Ρίχν-ω}{drop.\feats{imperf-pres.ind.1sg}}.}

%In glosses, morphological features for the components are optional, and it is a good practice to avoid overloading the example, thus only specifying those features %use only those
%which are relevant to the phenomenon discussed, as illustrated in example (\ref{el:drop-face}).

\ea\label{de:kopf-waschen}
\settowidth \jamwidth{(de)} 
\gll Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}.\\
Chris has Alex the  head  washed \\  \jambox{(de)}
\glt lit. `Chris washed Alex' hair.'\\
`Chris gave Alex a telling-off.' \\
\hspace*{\fill}(Höhle 2018: 9; our gloss and translation)
\z

The Farsi example in (\ref{fa:have-sleep-for-sb}) displays all items \ref{ex-line:use}–\ref{ex-line:idio-trans}. Item \ref{ex-line:source} may be omitted when the source of the example is not known, or when the example has been constructed. %by the authors. % Shortened to avoid orphan line
Items \ref{ex-line:lit-trans} and \ref{ex-line:idio-trans} can appear on the same line, separated by a vertical bar `|'.

\ea \label{fa:have-sleep-for-sb}
%\settowidth \jamwidth{(Farsi)} 
\settowidth \jamwidth{(fa)} 
\glll \PRL{است} \lex{\PRL{دیده}} \lex{\PRL{خواب}} \PRL{من} \lex{\PRL{برای}} \PRL{کافی} \PRL{قدر} \PRL{به} \\
ast \lex{dide} \lex{khab} man \lex{baraye} kafi qadre be  \\
is seen sleep me for enough quantity to\\ \jambox{(\cara{fa})}
\glt lit. `He had enough sleep for me.' | `He has many plans for me.’
\z

Farsi is a language which is written right to left, so the order of the words in item \ref{ex-line:use}, the Farsi sentence, is right to left. The transliteration in \ref{ex-line:transliteration}, however, is spelled left-to-right within each word.

The glosses for the examples in (\ref{de:kopf-waschen}) and (\ref{fa:have-sleep-for-sb}) are fully inflected English words. Sometimes it is desirable to use morpheme-by-morpheme glosses rather than inflected words, and the Leipzig glossing rules provide detailed instructions on how to do this. But the level of detail in the gloss should reflect the purpose of the example. When discussing MWEs, it is often irrelevant to include full morphological analyses of all the words in the sentence. In the Greek example in (\ref{el:drop-face}), only the person and number inflection of the verb and the possessive are shown; without this agreement the idiomatic reading of the MWE is lost.\footnote{The verb and its gloss could have been fully specified as \exgl[nl]{Ρίχν-ω}{drop.\feats{imperf-pres.ind.1sg}}.}

%\ea \label{sl:skrivati-glavo-v-pesek}
%\settowidth \jamwidth{(SL)} 
%\gll Ida \lex{skriva} \lex{glavo} \lex{v} \lex{pesek}. \\
%Ida hides head in sand \\ \jambox{(SL)}
%\glt lit. `Ida hides her head in the sand.' \\
%`Ida pretends not to see a problem.’
%\z

%\ea \label{el:take-decision}
%\settowidth \jamwidth{(EL)} 
%\glll Η Ζωή \lex{παίρνει} μία \lex{απόφαση}. \\
%i Zoi perni mia apofasi \\
%the\feats{.fem.sg} Zoe\feats{.fem.sg} take.\feats{3.sg} a decision \\ \jambox{(EL)}
%\glt lit. `Zoe takes a decision.' | `Zoe makes a decision.'
%\glt `Zoe takes a decision.' 
%\z

%\ea \label{el:drop-face}
%\settowidth \jamwidth{(EL)} 
%\glll \lex{Ρίχν-ω} \lex{τα} \lex{μούτρ-α} μου. \\
%riχn-o ta mutr-a mu\\
%riχn-o ta mutr-a mu\\
%\glt lit. `I drop my face' | `I suppress my dignity.'
%\z


% drop.IMPERF-PRES.IND.1SG  the.NEUT.PL.ACC  face-NEUT.PL.ACC  my.1SG
%\ea \label{el:drop-face-old}
%\settowidth \jamwidth{(EL)} 
%\cara{\glll \lex{Ρίχν-ω} \lex{τα} \lex{μούτρ-α} μου. \\
%Richn-o ta mutr-a mu\\
%drop-\feats{1sg} the.\feats{neut.pl.acc} face-\feats{neut.pl.acc} my.\feats{1sg}\\ \jambox{(EL)}
%\glt lit. `I drop my face.' | `I suppress my dignity.'}
%\z


\ea \label{el:drop-face}
\settowidth \jamwidth{(el)} 
\glll \lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου. \\
\cara{R}ichn-o ta mutra mu\\
drop-\feats{1sg} the face \feats{poss.1sg}\\ \jambox{(\cara{el})}
\glt lit. `I drop my face.' | `I suppress my dignity.'
\z

For English examples in English texts, most of the items \ref{ex-line:use}–\ref{ex-line:lit-trans} are irrelevant. 
But sometimes an idiomatic translation, item \ref{ex-line:idio-trans}, can be useful to ease comprehension for non-native readers, as in (\ref{en:take-on}).

\ea\label{en:take-on}
\settowidth \jamwidth{(en)} 
She reluctantly \lex{took} \lex{on} this task. \jambox{(\cara{en})}
\glt `She reluctantly undertook this task.'
\z

In all these examples the lexicalized components of the MWE, i.e., those which are realized by the same lexeme, are highlighted in boldface.\footnote{\url{https://parsemefr.lis-lab.fr/parseme-st-guidelines/1.2/?page=lexicalized}} 
%To avoid overloading the example, the conformity with the glossing standards mentioned in \ref{ex-line:gloss} can be lightened by specifying only those morphological features which are relevant to the phenomenon discussed, and omitting all others.
%These examples can be typeset using the dedicated LSP commands, \as{as illustrated in Appendix A}.
%, e.g. example (\ref{el:drop-face}) is typeset like this:

%\largerpage
%{\small
%\begin{verbatim}
%\ea \label{el:take-decision}
%\settowidth \jamwidth{(EL)} 
%\glll Η Ζωή \lex{παίρνει} μία \lex{απόφαση}. \\
%i Zoi perni mia apofasi \\
%the\feats{.fem.sg} Zoe\feats{.fem.sg} take.\feats{3.sg} a decision \\ 
%\jambox{(EL)}
%\glt `Zoe takes a decision.'
%\z
%\end{verbatim}
%\begin{verbatim}
%\ea \label{el:drop-face}
%\settowidth \jamwidth{(el)} 
%\glll \lex{Ρίχν-ω} \lex{τα} \lex{μούτρα} μου. \\
%Richn-o ta mutra mu\\
%drop-\feats{1sg} the face \feats{poss.1sg}\\ %\jambox{(el)}
%\glt lit. `I drop my face.' | `I suppress my dignity.'
%\z
%\end{verbatim}
%}

% Previous version
% \glt lit. `Zoe takes a decision.' | `Zoe makes a decision.'

\emph{Inline examples}, used for brevity, are preceded by the lowercase two- or three-letter ISO 639 code in parentheses, and contain only a subset of items \ref{ex-line:use}--\ref{ex-line:idio-trans}.
%\ref{ex-line:use}--\ref{ex-line:source}.
%, so as, preferably , not to exceed two lines of text in total. 
Items \ref{ex-line:transliteration}--\ref{ex-line:lit-trans}, if present, occur in parentheses and are separated by a vertical bar `|'. Inline examples exceeding one line of text are discouraged.

If the language uses Latin script, item \ref{ex-line:use} in inline examples is always spelled in italics. 
For instance, an inline example corresponding to numbered example (\ref{de:kopf-waschen}) would be as follows: (de) \exgllitidio{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{Chris has Alex the head  washed}{Chris washed Alex' hair}{Chris gave Alex a telling-off}. The same example omitting the gloss would be (de) \exlitidio{Chris hat  Alex \lex{den} \lex{Kopf} \lex{gewaschen}}{Chris washed Alex' hair}{Chris gave Alex a telling-off}.
%The inline example corresponding to (\ref{sl:skrivati-glavo-v-pesek}), but omitting the gloss, would be: (SL) \exlitidio{Ida \lex{skriva} \lex{glavo} \lex{v} \lex{pesek}}{Ida hides her head in the sand}{Ida pretends not to see a problem}. 

If the language under study is written in non-Latin script, %alphabet, 
item \ref{ex-line:use}  should not be in italics, and the transliteration should be included in parentheses (and followed by a vertical bar `|' if needed). For instance, the use example,
%, %transliteration, 
gloss, literal translation and idiomatic translation of example (\ref{el:drop-face}) would be: 
%(EL) \extlilitidio[nl]{Η Ζωή \lex{παίρνει} μία \lex{απόφαση}}{I Zoi perni mia apofasi}{the Zoe takes a decision}{Zoe makes a decision}.
%(EL) \extlilitidio[nl]{\lex{Ρίχνω τα μούτρα} μου}{riχno ta mutra mu}{I drop my face}{I suppress my dignity}.
%(\cara{el}) \extlilitidio[nl]{\lex{Ρίχνω τα μούτρα} μου}{drop.\feats{1sg} the face.\feats{acc} mine.\feats{gen}}{I drop my face}{I suppress my dignity}. => commented out on Jul 22, 2020
(el) \extlilitidio[nl]{\lex{Ρίχν-ω τα μούτρα} μου}{drop-\feats{1sg} the face \feats{poss.1sg}}{I drop my face}{I suppress my dignity}.

To keep such examples reasonably short, the first item can be omitted, keeping only the transliteration: %ed example is kept: 
%(EL) \exlitidio{I Zoi \lex{perni} mia \lex{apofasi}}{the Zoe takes a decision}{Zoe makes a decision}. 
(el) \exlitidio{\lex{Richno ta mutra} mu}{I drop my face}{I suppress my dignity}.
Literal and idiomatic translations %are sometimes 
may be too verbose or superfluous, and can be skipped: %, as in:  
%\exidio[nl]{Η Ζωή \lex{παίρνει} μία \lex{απόφαση}}{Zoe makes a decision}.
(el) \exidio[nl]{\lex{Ρίχνω τα μούτρα} μου}{I suppress my dignity}.

The \LaTeX~typesetting commands for both numbered and inline examples can be found in the LSP templates and in the source codes attached to these guidelines.\footnote{{\scriptsize \url{URL TO UPDATE} file \texttt{multilingual-mwe-examples.tex}}} Examples of use for these commands are given in Appendices A and B.

\appendix
%Testing commands with 1 parameter
%\vspace{2cm}
%\textbf{TESTING ALL THE COMMANDS} %\textcolor{red}{Comment out in the final version!} \\

%\pagebreak
%\section*{Appendix A: Source codes for numbered examples}
\section*{Appendix A: \LaTeX{} commands for numbered examples}
\input{app-numbered}

%\pagebreak
\section*{Appendix B: \LaTeX{} commands for inline examples}
%\section*{Appendix B: Illustration of inline examples}
\input{app-inline}








%\noindent

\end{document}

