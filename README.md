# PMWE

This the Gitlab space for [Phraseology and Multiword Expressions](https://langsci-press.org/catalog/series/pmwe), a book series at Language Science Press.
See our [Wiki pages](https://gitlab.com/parseme/pmwe/-/wikis/home) for more explanations.
